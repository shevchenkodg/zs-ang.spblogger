'use strict'

angular.module('spBlogger.posts.services', []).factory('postService', function() {
	return {
		posts: [{
			id: 1,
			title: 'Simple title1',
			content: 'Sample content...',
			permalink: 'simple-title1',
			author: 'Shevchenko',
			datePublished: '2016-03-11'

		}, {
			id: 2,
			title: 'Simple title2',
			content: 'Sample content...',
			permalink: 'simple-title2',
			author: 'Shevchenko',
			datePublished: '2016-03-11'
		}, {
			id: 3,
			title: 'Simple title3',
			content: 'Sample content...',
			permalink: 'simple-title3',
			author: 'Shevchenko',
     		datePublished: '2016-03-11'
		}, {
			id: 4,
			title: 'Simple title4',
			content: 'Sample content...',
			permalink: 'simple-title4',
			author: 'Shevchenko',
			datePublished: '2016-03-11'
		}],
		getAll: function() {
			return this.posts;
		},
		getPostById: function(id) {
			for (var i in this.posts) {
				if (this.posts[i].id == id) {
					return this.posts[i];
				}
			}
		},
	}
});