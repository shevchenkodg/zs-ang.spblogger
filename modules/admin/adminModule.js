'use strict';

angular.module('spBlogger.admin',[
	'spBlogger.admin.controllers',
]);

angular.module('spBlogger.admin')
.config(['$stateProvider', function($stateProvider){
	$stateProvider
	.state('admin', {
		url: '/admin',
		controller: 'AdminController',
		templateUrl: 'modules/admin/views/admin-home.html'
	})
	.state('admin.postNew', {
		url: '/posts/new',
		controller: 'PostCreationController',
		templateUrl: 'modules/admin/views/admin-new-post.html'
	});
}]);