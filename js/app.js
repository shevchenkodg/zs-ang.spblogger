'use strict';

angular.module('spBlogger',[
	'ui.router',
	'spBlogger.admin',
	'spBlogger.posts',
	'spBlogger.directives',
]);

angular.module('spBlogger').value('version','V1.0');

angular.module('spBlogger').run(['$state', function($state) {
	$state.go('allPosts');
}]);